<?php


namespace App\Services;


use App\Contract\NSContract;

abstract class BaseAPI implements NSContract
{
    const STATIONS_ENDPOINT = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/stations';
    const DEPARTURES_ENDPOINT = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/departures';
    const TRIP_ENDPOINT = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v3/trips';
    const DISRUPTIONS_ENDPOINT = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v3/disruptions';

    protected string $primaryNSKEy = '54a8019c732747e298f9a0af30c11ccd';
    protected string $pricesKey = '72086b289d9e4417acb2fef2fc2995d1';
    protected array  $departuresParams = [];
    protected array  $tripsParams = [];
    protected array  $disruptionsParams = [];

    /**
     * @return array
     */
    public function getDisruptionsParams(): array
    {
        return $this->disruptionsParams;
    }

    /**
     * @param array $disruptionsParams
     */
    public function setDisruptionsParams(array $disruptionsParams): void
    {
        $this->disruptionsParams = $disruptionsParams;
    }

    /**
     * @return array
     */
    public function getTripsParams(): array
    {
        return $this->tripsParams;
    }

    /**
     * @param array $tripsParams
     */
    public function setTripsParams(array $tripsParams): void
    {
        $this->tripsParams = $tripsParams;
    }
    protected array  $pricesParams = [];


    /**
     * @return array
     */
    public function getDeparturesParams(): array
    {
        return $this->departuresParams;
    }

    /**
     * @param array $departuresParams
     */
    public function setDeparturesParams(array $departuresParams): void
    {
        $this->departuresParams = $departuresParams;
    }

    /**
     * @return string
     */
    public function getPricesKey(): string
    {
        return $this->pricesKey;
    }

    /**
     * @param string $pricesKey
     */
    public function setPricesKey(string $pricesKey): void
    {
        $this->pricesKey = $pricesKey;
    }

    /**
     * @return array|string[]
     */
    public function getPricesParams()
    {
        return $this->pricesParams;
    }

    /**
     * @param array|string[] $pricesParams
     */
    public function setPricesParams($pricesParams): void
    {
        $this->pricesParams = $pricesParams;
    }

    /**
     * @return array|bool[]
     */
    public function getIncidentsParams()
    {
        return $this->incidentsParams;
    }

    /**
     * @param array|bool[] $incidentsParams
     */
    public function setIncidentsParams($incidentsParams): void
    {
        $this->incidentsParams = $incidentsParams;
    }
    protected array $incidentsParams = ['actual' => true];

    /**
     * @return string
     */
    public function getPrimaryNSKEy(): string
    {
        return $this->primaryNSKEy;
    }

    /**
     * @param string $primaryNSKEy
     */
    public function setPrimaryNSKEy(string $primaryNSKEy): void
    {
        $this->primaryNSKEy = $primaryNSKEy;
    }
}