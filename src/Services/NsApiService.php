<?php
namespace App\Services;

use RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class NsApiService extends BaseAPI
{
  const DEPARTURE_DATA = 'departure_data';
  const TRIP_DATA = 'trip_data';
  const DISRUPTION_DATA = 'disruption_data';

  private string $typeDisruptions = 'MAINTENANCE';

  public function getAllStations(){
      $client = HttpClient::create();
      try {
          $response = $client->request('GET',
              self::STATIONS_ENDPOINT
              ,
              [
                  'headers' => [
                      'Ocp-Apim-Subscription-Key' => $this->getPrimaryNSKEy(),
                  ]
              ]
          );
      } catch (TransportExceptionInterface $e) {
          throw new RuntimeException('Unable to retrieve data!');
      }

      $resData = [];
      if (!empty($response)) {
          try {
              $resData = $response->toArray();
          }
          catch (RuntimeException $e) {
              $e->getMessage();
          }
      }


      return $resData && $resData['payload'] ? $resData['payload'] : [];
  }

    public function getDepartures($params){
        $this->setDeparturesParams($params);
        $client = HttpClient::create();
        try {
            $response = $client->request('GET',
                self::DEPARTURES_ENDPOINT
                ,
                [
                    'headers' => [
                        'Ocp-Apim-Subscription-Key' => $this->getPrimaryNSKEy(),
                    ],
                    'query' => [
                        'station' => $this->getStationCode($this->getDeparturesParams()['station']??'')
                    ]
                ]
            );
        } catch (TransportExceptionInterface $e) {
            throw new RuntimeException('Unable to retrieve data!');
        }

        $resData = [];
        if (!empty($response)) {
            try {
                $resData = $response->toArray();
            }
            catch (RuntimeException $e) {
                $e->getMessage();
            }
        }

        if (isset($resData['payload'])){
            array_push($resData['payload']['departures'], ['type_data' => self::DEPARTURE_DATA]);
        }

        return $resData && $resData['payload'] ? $resData['payload']['departures'] : [];
    }

    public function getTrips($params){
        $this->setTripsParams($params);
        if (!isset($this->getTripsParams()['inputVan']) || !isset($this->getTripsParams()['inputNaar'])){
            return 0;
        }
        $dateAndTime = '';
        if ($this->getTripsParams()
            && isset($this->getTripsParams()['inputDate'])
            && isset($this->getTripsParams()['inputTime']))
        {
            $dateAndTime =
                new \DateTime($this->getTripsParams()['inputDate'] .' '. $this->getTripsParams()['inputTime']);
        }

        $client = HttpClient::create();
        try {
            $response = $client->request('GET',
                self::TRIP_ENDPOINT
                ,
                [
                    'headers' => [
                        'Ocp-Apim-Subscription-Key' => $this->getPrimaryNSKEy(),
                    ],
                    'query' =>  [
                      'fromStation' => $this->getStationCode($this->getTripsParams()['inputVan']),
                        'toStation' => $this->getStationCode($this->getTripsParams()['inputNaar']),
                        'dateTime' =>  $dateAndTime ? $dateAndTime->format(\DateTime::RFC3339) : '',
                        'searchForArrival' =>
                            isset($this->getTripsParams()['flexRadioDefault']) && $this->getTripsParams()['flexRadioDefault'] === 'departure' ? false : true
                    ]
                ]
            );
        } catch (TransportExceptionInterface $e) {
            throw new RuntimeException('Unable to retrieve data!');
        }
        $resData = [];
        if (!empty($response)) {
            try {
                $resData = $response->toArray();
            }
            catch (RuntimeException $e) {
                $e->getMessage();
            }
        }

        if (isset($resData['trips'])){
            array_push($resData['trips'], ['type_data' => self::TRIP_DATA]);
        }

        return $resData && $resData['trips'] ? $resData['trips'] : [];
    }

    public function getDisruptions($params){
      $this->setDisruptionsParams($params);
        $client = HttpClient::create();
        try {
            $response = $client->request('GET',
                self::DISRUPTIONS_ENDPOINT
                ,
                [
                    'headers' => [
                        'Ocp-Apim-Subscription-Key' => $this->getPrimaryNSKEy(),
                    ],
                    'query' => [
                        'station' => $this->getStationCode($this->getDeparturesParams()['station']??$this->getTripsParams()['inputVan']),
                        'isActive' => true,
                        'type' => $this->typeDisruptions
                    ]
                ]
            );
        } catch (TransportExceptionInterface $e) {
            throw new RuntimeException('Unable to retrieve data!');
        }
        $resData = [];
        if (!empty($response)) {
            try {
                $resData = $response->toArray();
            }
            catch (RuntimeException $e) {
                $e->getMessage();
            }
        }

        return $resData  ? $resData : [];

    }
 /**
  * @todo this can be refactored to store stations in db, instead of extra api calls then searching the array!
 **/
  private function getStationCode($station){
      foreach ($this->getAllStations() as $key=>$val)
      {
          $aNames[] = $val['namen'];
          $s = ucfirst($station);
          if (in_array( $s, array_column($aNames, 'kort'))
              || in_array( $s, array_column($aNames, 'middel'))
              || in_array( $s, array_column($aNames, 'lang'))){
              return $val['code'];
          }
      }
  }
}