<?php


namespace App\Services;


use Symfony\Component\HttpFoundation\Request;

class FormValidatorService
{
    private $request;

    public function __construct(Request $request){
        $this->request = $request;
    }

    public function handleForm(){
        $data = [];
        if($this->request)
          return  $data = $this->validateForm($this->request);

    }

    public function isSubmitted(Request $request){
        return $request->getMethod() === "POST";
    }

    private function validateForm(Request $request){
        $inputs = [];
        if ($this->isSubmitted($request)) {
            $arrInputs = $request->request->all();
            foreach ($arrInputs as $key => $value) {
                $inputs[$key] = $this->sanitizeInput($value);
            }
        }
        return $inputs;
    }

    private function sanitizeInput($input){
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }
}