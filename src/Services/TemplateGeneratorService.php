<?php


namespace App\Services;


error_reporting(E_ALL ^ E_WARNING);

class TemplateGeneratorService
{
    private ?array $resData;
    private ?array $displayOptions;
    private int $disruptions = 0;

    public function __construct(?Array $data = [], ?Array $options = []){
     $this->resData = $data;
     $this->displayOptions = $options;
    }

const HEADER_TEMPLATE =
'
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Demo app</title>

    <link rel="canonical" href="">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../../public/assets/main.css" type="text/css" rel="stylesheet">
</head>

<style>
body {
    background: url(https://i.picsum.photos/id/155/3264/2176.jpg?hmac=Zgf_oGMJeg18XoKBFmJgp2DgHMRYuorXlDx5wLII9nU) repeat-y center center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.form-planing {
    width: 100%;
    max-width: 340px;
    height: 480px;
    padding: 15px;
    margin: 0 auto;
    background-color: whitesmoke;
    border-radius: 15px;
}

.table-style{
    background-color: whitesmoke;
    border-radius: 15px;
}
</style>

<body class="text-center">
<!-- navigation header -->
<nav class="navbar navbar-expand-md navbar-light  bg-light justify-content-between">
    <a class="navbar-brand" href="#">92/92 Demo App</a>

    <div>
        <form id="searchForm" class="form-inline mt-2 mt-md-0" method="POST" action="">
            <input name="station" class="form-control  form-control-sm mr-1" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-success-outline btn-sm my-2 my-sm-0" formmethod="post" type="submit">Search</button>
        </form>
    </div>
</nav>
<!-- end navigation header -->
';
public string $dynamicContent = '';

const FOOTER_TEMPLATE =
'
<footer class="footer mt-auto py-3 bg-light" style="margin-top: 110px !important;">
    <div class="container">
    <span class="text-muted">© 2022 Demo:
    <a class="text-reset fw-bold" href="https://gitlab.com/Adel-Bourguiba/mxe-ns-demo">Git repository</a></span>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
';

    /**
     * @return string
     */
public function getDynamicContent(): string
{
$this->dynamicContent = '
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div id="exTab1" class="form-planing mt-5">
                <ul  class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link"  href="#1a" data-toggle="tab">Planner</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#2a" data-toggle="tab">Verstoringen</a>
                    </li>
                </ul>

                <div class="tab-content clearfix">
                    <div class="tab-pane active" id="1a">
                        <form id="planForm" class="mt-3" method="POST" action="">

                            <h1 class="h3 mb-3 font-weight-normal">Waar wil je heen?</h1>
                            <div class="form-group">
                                <label for="inputVan" class="sr-only">Van</label>
                                <input type="text" name="inputVan" id="inputVan" class="form-control" placeholder="Van: Adres, station, postcode, etc." required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="inputNaar" class="sr-only">Naar</label>
                                <input type="text" name="inputNaar" id="inputNaar" class="form-control" placeholder="Naar: Adres, station, postcode, etc." required>
                            </div>
                            <div class="form-group">
                                <label for="inputDate" class="sr-only">Datum</label>
                                <input type="date" name="inputDate" id="inputDate" class="form-control" placeholder="Datum">
                            </div>
                            <div class="form-group">
                                <label for="inputTime" class="sr-only">Tijd</label>
                                <input type="time" name="inputTime" id="inputTime" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="sr-only">Type reis</label>
                                <input class="form-check-input" type="radio" value="departure" name="flexRadioDefault" id="flexRadioDefault1" checked>
                                <label class="form-check-label mr-5" for="flexRadioDefault1">
                                    Vertrek
                                </label>
                                <input class="form-check-input" type="radio" value="arrival" name="flexRadioDefault" id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Aankomst
                                </label>
                            </div>


                            <button class="btn btn-lg btn-secondary btn-block"
                                    type="submit" formmethod="post" formaction="">Plan mijn reis    >></button>
                        </form>
                    </div>
                    <div class="tab-pane" id="2a" style="overflow: scroll; height: 420px">
                        <h6>Aantal verstoringen ('. count($this->resData[1]) .')</h6>
                        
                        '. $this->renderCards($this->resData) .'
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-7 mt-5 '. $this->displayOptions['show-table'].' ">
            <table class="table table-striped table-style">
                <thead>
                 <tr>
                    <th colspan="7">Vertrek: '. $this->displayOptions['departure'].$this->displayOptions['tripFrom']
                     . $this->displayOptions['tripTo']  .'</th>
                 </tr>
                   '.$this->renderTableHeader().'
                </thead>
                <tbody>  
                  '. $this->renderRow($this->resData) .'
                </tbody>
            </table>
        </div>
    </div>
</div>
';

return $this->dynamicContent;
}


private function renderRow($data){
if (!$data){
return '<tr>
   <td colspan="7">Er is geen informatie voor uw verzoek</td>
</tr>   
';
}
$tableRows = '';
$departures = $data[0];
if (end($departures)['type_data'] === NsApiService::DEPARTURE_DATA){
    array_pop($departures);
    for ($i = 0; $i < count($departures); $i++) {
        $tableRows .=
            '<tr>
           <td>'. $this->formatDate($departures['plannedDateTime']) .'</td>
           <td>'. $departures[$i]['direction'] .'</td>
           <td>'. $departures[$i]['plannedTrack'] .'</td>
           <td>'. count($departures[$i]['routeStations']) .'</td>
           <td>'. $this->isCancelled($departures[$i]['cancelled']) .'</td>
        </tr>
';
    }
}

$trips = $data[0];
if (end($trips)['type_data'] === NsApiService::TRIP_DATA){
    array_pop($trips);
    for ($i = 0; $i < count($trips); $i++) {
        $tableRows .=
            '<tr>          
               <td>'. $this->formatDate($trips[$i]['legs'][0]['origin']['plannedDateTime']) .'</td>
               <td>'. $this->formatDate($trips[$i]['legs'][0]['destination']['plannedDateTime']) .'</td>
               <td>'. $trips[$i]['legs'][0]['origin']['plannedTrack'] .'</td>
               <td>'. $trips[$i]['actualDurationInMinutes'] . " min" .'</td>         
               <td>'.  number_format((float)$trips[$i]['fares'][1]['priceInCents']/100, 2, '.', '') ." €".'</td>
            </tr>
';
    }
}
    return $tableRows;
}

private function renderCards($data){
    if (!$data){
        return '<div>
                     <h4>Er is geen informatie voor uw verzoek</h4>
                </div>   
';
    }
    $card = '';
    $disruptions = $data[1];
    $this->disruptions = isset($disruptions) ? count($disruptions) : 0;

    if (isset($disruptions)){
        for ($i = 0; $i < count($disruptions); $i++) {
            $card .=
                '          
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">'. $disruptions[$i]['title'] .'</h5>
                            <h6 class="card-subtitle mb-2 text-muted">'. $disruptions[$i]['period'] .'</h6>
                            <p class="card-text">'. $disruptions[$i]['summaryAdditionalTravelTime']['label'] .'</p>
                        </div>
                    </div>
';
        }
    }
    return $card;
}

public function renderTableHeader(){
    if ($this->displayOptions['tripTo']){
        return '  <tr>
                        <th scope="col">Vertrektijd</th>
                        <th scope="col">Aankomsttijd</th>              
                        <th scope="col">Spoor</th>
                        <th scope="col">Reisduur</th>              
                        <th scope="col">Prijs (tweede klas)</th>
                    </tr>'
            ;
    }
    return '  <tr>
                        <th scope="col">Tijd</th>
                        <th scope="col">Richting</th>              
                        <th scope="col">Spoor</th>
                        <th scope="col">Stops</th>              
                        <th scope="col">Geannuleerd</th>
                    </tr>'
        ;
}
private function formatDate($date){
    $date=date_create($date);
    return date_format($date,"H:i");
}
private function isCancelled($cancelled){
    return $cancelled===1?"JA":"NEE";
}
}