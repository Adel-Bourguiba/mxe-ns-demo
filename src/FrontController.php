<?php

namespace App;

use App\Services\FormValidatorService;
use App\Services\NsApiService;
use App\Services\TemplateGeneratorService;
use Symfony\Component\HttpFoundation\Request;


class FrontController
{
    /**
     * @param Request $request
     * @return string
     */
    public function generateHtmlResponse(Request $request)
    {
        $params = $this->getValidatedParams($request);
        $nsService = new NsApiService();
        $tripsData = $nsService->getTrips($params);
        $departuresData = $nsService->getDepartures($params);
        $disruptionsData = $nsService->getDisruptions($params);

        $displayOptions ['show-table'] = $request->getMethod() === 'GET' ? 'd-none' : '';

        if (gettype($tripsData) === 'array'){
            $displayOptions ['tripFrom'] = isset($params['inputVan']) ? ucfirst($params['inputVan']) :'';
            $displayOptions ['tripTo'] = isset($params['inputNaar']) ? "- Aankomst: " . ucfirst($params['inputNaar']) :'';
            $template = new TemplateGeneratorService([$tripsData, $disruptionsData], $displayOptions);
        }else{
            $displayOptions ['departure'] = isset($params['station']) ? ucfirst($params['station']) :'';
            $template = new TemplateGeneratorService([$departuresData,$disruptionsData], $displayOptions);
        }

        return $template->getDynamicContent();
    }

    /**
     * @param Request $request
     * @return array
     */
    private function getValidatedParams(Request $request){
        if (!$request){
            return [];
        }
        $validator = new FormValidatorService($request);
        return $validator->handleForm();
    }
}