FROM php:8.0-apache

RUN apt-get update && apt-get install -y --no-install-recommends \
        nano \
        libmagickwand-dev \
        build-essential \
        git \
        htop \
        libicu-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        ruby \
        libmemcached-dev \
        ruby-dev \
        qrencode \
        cmake \
        gnupg \
        wkhtmltopdf \
        xvfb \
        percona-toolkit \
        wget \
        npm \
        unzip

# NODEJS
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install nodejs -y
RUN nodejs -v
RUN npm -v


# MISC
RUN docker-php-ext-install -j$(nproc) iconv
RUN docker-php-ext-install opcache
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install exif
RUN docker-php-ext-install intl
RUN docker-php-ext-install sockets

# APCU
RUN pecl install apcu
RUN docker-php-ext-enable apcu

# APACHE
RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod ssl

# COMPOSER
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

# PHP SETTINGS
RUN echo 'date.timezone=Europe/Amsterdam' > /usr/local/etc/php/conf.d/timezone-php.ini

# LOCATE
RUN apt-get install locate -y

EXPOSE 443
EXPOSE 80