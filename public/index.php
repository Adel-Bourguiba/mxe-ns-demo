<?php

require_once dirname(__DIR__).'/public/bootstrap.php';
use App\FrontController;
use App\Services\TemplateGeneratorService;
use Symfony\Component\HttpFoundation\Request;

$controller = new FrontController();
$templateGenerator = new TemplateGeneratorService([], []);

echo $templateGenerator::HEADER_TEMPLATE;
echo $controller->generateHtmlResponse(Request::createFromGlobals());
echo $templateGenerator::FOOTER_TEMPLATE;



